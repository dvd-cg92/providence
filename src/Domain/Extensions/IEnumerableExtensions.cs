﻿using Domain.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Extensions
{
    public static class IEnumerableExtensions
    {
        public static bool IsNullOrEmpty<T>(this IEnumerable<T> value)
        {
            return value == null || !value.Any();
        }

        /// <summary>
        /// Gets a page of items in the IEnumerable.
        /// </summary>
        /// <typeparam name="T">The type of the items in the IEnumerable</typeparam>
        /// <param name="value">The IEnumerable to iterate.</param>
        /// <param name="page">The number of the page to retrieve.</param>
        /// <param name="size">The amount of items per page.</param>
        /// <returns>The requested page.</returns>
        public static IEnumerable<T> GetPage<T>(this IEnumerable<T> value, int page, int size)
        {
            return value
                .Skip(page * size)
                .Take(size);
        }

        /// <summary>
        /// Gets a subset of IEnumerable items by a given page and gap index.
        /// </summary>
        /// <typeparam name="T">The type of the items in the IEnumerable</typeparam>
        /// <param name="value">The IEnumerable to iterate.</param>
        /// <param name="page">The number of the page to retrieve.</param>
        /// <param name="pageSize">The amount of items per page.</param>
        /// <returns>The offset.</returns>
        public static IEnumerable<T> GetOffset<T>(this IEnumerable<T> value, int page, int pageSize, int index, int offset)
        {
            return value
                .Skip(page * pageSize + (index - 1) * offset)
                .Take(offset);
        }

        /// <summary>
        /// Gets the amount of pages in an IEnumerable.
        /// </summary>
        /// <typeparam name="T">The type of the items in the IEnumerable</typeparam>
        /// <param name="value">The IEnumerable to iterate.</param>
        /// <param name="size">The amount of items per page.</param>
        /// <returns>The amount of pages in an IEnumerable.</returns>
        public static int GetPageAmount<T>(this IEnumerable<T> value, int size)
        {
            return Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(value.Count()) / Convert.ToDecimal(size)));
        }

        /// <summary>
        /// This will execute an action over an enumerable page by page. Several pages can be executed in paralel.
        /// Page size and the amount of concurrent tasks are configured in the input.
        /// </summary>
        /// <typeparam name="T">The output is an IEnumerable of this type.</typeparam>
        /// <typeparam name="U">The input is an IEnumerable of this type.</typeparam>
        /// <param name="value">The IEnumerable of U where the iteration is performed.</param>
        /// <param name="pageSize">The amount of items in each page.</param>
        /// <param name="concurrentTasks">The amount of concurrent tasks.</param>
        /// <param name="func">The action to perform for each page.</param>
        /// <returns>An agregation of the results of each action.</returns>
        public static async Task<IEnumerable<T>> ForEachPage<T, U>(this IEnumerable<U> value
            , int pageSize
            , int concurrentTasks
            , Func<IEnumerable<U>, Task<T>> func)
        {
            List<T> results = null;

            int p = 0;

            if (!value.IsNullOrEmpty())
            {
                results = new List<T>();

                IEnumerable<U> page = value.GetPage(p, pageSize);

                List<Task<T>> tasks = new List<Task<T>>();

                while (!page.IsNullOrEmpty())
                {
                    tasks.Add(func(page));

                    if (tasks.Count == concurrentTasks)
                    {
                        results.AddRange((await Task.WhenAll(tasks).ConfigureAwait(false)).Where(r => r != null));

                        tasks = new List<Task<T>>();
                    }

                    p++;

                    page = value.GetPage(p, pageSize);
                }

                if (tasks.Count > 0)
                {
                    results.AddRange((await Task.WhenAll(tasks).ConfigureAwait(false)).Where(r => r != null));
                }
            }

            return results;
        }

        //public static string GetCacheKey(this IEnumerable<ICacheKey> value)
        //{
        //    string ret = "";
        //    if (!value.IsNullOrEmpty())
        //    {
        //        ret = string.Join("#", value.OrderBy(v => v.GetCacheKey()).Select(v => v.GetCacheKey())).ToMd5Hash();
        //    }

        //    return ret;
        //}

        /// <summary>
        /// Filters elements to remove outliers. 
        /// The outliers are these elements which are further from an average than k*(standard deviation). 
        /// Set k=3 for standard three-sigma rule.
        /// </summary>
        public static IEnumerable<T> SkipOutliers<T>(this IEnumerable<T> enumerable, decimal k, Func<T, decimal> selector)
        {
            decimal average = enumerable.Average(selector);
            decimal StandDesviation = enumerable.StandardDeviation(selector);
            decimal sigma = k * StandDesviation;

            //We should Add a parameter to avoid skip min value or max value (skip max for transport prices, or skip min for hotel rating). 
            //Removing the Math.Abs shoul be enough
            foreach (T item in enumerable)
            {
                if (Math.Abs(selector(item) - average) <= sigma)
                    yield return item;
            }
        }

        /// <summary>
        /// Calculates a standard deviation of elements
        /// </summary>
        public static decimal StandardDeviation<T>(this IEnumerable<T> enumerable, Func<T, decimal> selector)
        {
            decimal sum = 0;

            decimal average = enumerable.Average(selector);

            int n = 0;

            foreach (T item in enumerable)
            {
                decimal diff = selector(item) - average;

                sum += diff * diff;

                n++;
            }

            return n == 0 ? 0 : Convert.ToDecimal(Math.Sqrt(Convert.ToDouble(sum) / n));
        }

        public static IEnumerable<T> Flatten<T>(this IEnumerable<IEnumerable<T>> value)
        {
            List<T> flattened = null;

            if (!value.IsNullOrEmpty())
            {
                flattened = new List<T>();

                foreach (IEnumerable<T> list in value)
                {
                    if (!list.IsNullOrEmpty())
                    {
                        flattened.AddRange(list);
                    }
                }
            }

            return flattened;
        }

        public static Dictionary<T, IEnumerable<I>> GetDictionaryWithDistinctValues<T, I>(
            this IEnumerable<KeyValuePair<T, IEnumerable<I>>> values)
        {
            Dictionary<T, IEnumerable<I>> dictionary = new Dictionary<T, IEnumerable<I>>();

            foreach (KeyValuePair<T, IEnumerable<I>> value in values)
            {
                if (!dictionary.ContainsKey(value.Key))
                {
                    dictionary.Add(value.Key, value.Value);
                }
            }

            return dictionary;
        }

        public static IEnumerable<KeyValuePair<T, IEnumerable<I>>> GetKeyValuePairsWithDistinctValues<T, I>(
            this IEnumerable<KeyValuePair<T, IEnumerable<I>>> values)
        {
            List<KeyValuePair<T, IEnumerable<I>>> distinctValues = new List<KeyValuePair<T, IEnumerable<I>>>();

            foreach (KeyValuePair<T, IEnumerable<I>> value in values)
            {
                if (distinctValues.All(d => d.Key.ToString() != value.Key.ToString())
                    && distinctValues.All(d =>
                    {
                        bool notContainsAll = false;
                        foreach (var val in d.Value)
                        {
                            if (!value.Value.Contains(val))
                            {
                                notContainsAll = true;
                                break;
                            }
                        }

                        return notContainsAll;
                    }))
                {
                    distinctValues.Add(value);
                }
            }

            return distinctValues;
        }

        public static IEnumerable<KeyValuePair<T, List<I>>> GetKeyValuePairsWithDistinctValues<T, I>(
            this IEnumerable<KeyValuePair<T, List<I>>> values)
        {
            List<KeyValuePair<T, List<I>>> distinctValues = new List<KeyValuePair<T, List<I>>>();

            foreach (KeyValuePair<T, List<I>> value in values)
            {
                if (distinctValues.All(d => d.Key.ToString() != value.Key.ToString())
                    && distinctValues.All(d =>
                    {
                        bool notContainsAll = false;
                        foreach (var val in d.Value)
                        {
                            if (!value.Value.Contains(val))
                            {
                                notContainsAll = true;
                                break;
                            }
                        }

                        return notContainsAll;
                    }))
                {
                    distinctValues.Add(value);
                }
            }

            return distinctValues;
        }
    }
}
