﻿using System;

namespace Domain.Exceptions
{
    public class ControlledException : Exception
    {
        public string ClientMessage { get; set; }
        public string ErrorType { get; set; }

        public ControlledException(string exception, string clientMessage, string errorType) : base(exception)
        {
            ErrorType = errorType;
            ClientMessage = clientMessage;
        }

        public ControlledException(string exception, string clientMessage) : base(exception)
        {
            ClientMessage = clientMessage;
        }

        public ControlledException(string exception) : base(exception)
        {
        }
    }
}
