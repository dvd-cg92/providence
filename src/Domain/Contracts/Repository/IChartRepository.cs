﻿using Domain.Entities.GetChart;
using System.Threading.Tasks;

namespace Domain.Contracts.Repository
{
    public interface IChartRepository
    {
        Task<GetChartRs> GetChart(string ticker, string interval);
    }
}
