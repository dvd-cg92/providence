﻿using System.Collections.Generic;

namespace Domain.Entities.GetChart
{
    public class GetChartRs
    {
        public ChartData Chart { get; set; }

        public class TradingPeriod
        {
            public string Timezone { get; set; }
            public int Start { get; set; }
            public int End { get; set; }
            public int Gmtoffset { get; set; }
        }

        public class CurrentTradingPeriod
        {
            public TradingPeriod Pre { get; set; }
            public TradingPeriod Regular { get; set; }
            public TradingPeriod Post { get; set; }
        }

        public class TradingPeriods
        {
            public List<List<TradingPeriod>> Pre { get; set; }
            public List<List<TradingPeriod>> Regular { get; set; }
            public List<List<TradingPeriod>> Post { get; set; }
        }

        public class Meta
        {
            public string Currency { get; set; }
            public string Symbol { get; set; }
            public string ExchangeName { get; set; }
            public string InstrumentType { get; set; }
            public int FirstTradeDate { get; set; }
            public int RegularMarketTime { get; set; }
            public int Gmtoffset { get; set; }
            public string Timezone { get; set; }
            public string ExchangeTimezoneName { get; set; }
            public double RegularMarketPrice { get; set; }
            public double ChartPreviousClose { get; set; }
            public double PreviousClose { get; set; }
            public int Scale { get; set; }
            public int PriceHint { get; set; }
            public CurrentTradingPeriod CurrentTradingPeriod { get; set; }
            public TradingPeriods TradingPeriods { get; set; }
            public string DataGranularity { get; set; }
            public string Range { get; set; }
            public List<string> ValidRanges { get; set; }
        }

        public class Quote
        {
            public List<double> Close { get; set; }
            public List<double> High { get; set; }
            public List<int> Volume { get; set; }
            public List<double> Low { get; set; }
            public List<double> Open { get; set; }
        }

        public class Indicators
        {
            public Quote[] Quote { get; set; }
        }

        public class Result
        {
            public Meta Meta { get; set; }
            public int[] Timestamp { get; set; }
            public Indicators Indicators { get; set; }
        }

        public class ChartData
        {
            public List<Result> Result { get; set; }
            public string Error { get; set; }
        }
    }
}
