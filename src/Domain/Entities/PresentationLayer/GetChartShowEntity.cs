﻿using System.Collections.Generic;

namespace Domain.Entities.PresentationLayer
{
    public class GetChartShowEntity
    {
        public List<ChartDataItem> ChartData { get; set; }
    }
}
