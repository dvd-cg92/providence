﻿using System;

namespace Domain.Utils.Http.Contracts
{
    public interface ICacheResponse
    {
        public bool IsValidForCache { get; }
        public bool IsValidForAvailability { get; }
        public Guid Token { get; }
        public string GeneratedCacheKey { get; set; }
        public bool RetrievedFromCache { get; set; }
    }
}
