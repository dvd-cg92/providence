﻿using Domain.Entities.HttpClient.Entities;
using System.Net;

namespace Domain.Entities.HttpClient.Extensions
{
    public static class RichHttpClientResponseExtensions
    {
        public static bool IsNullOrUndefined<T>(this RichHttpClientResponse<T> value)
        {
            return value == null || value.Value == null;
        }

        public static bool IsValidValue<T>(this RichHttpClientResponse<T> value)
        {
            return value != null && value.StatusCode == HttpStatusCode.NoContent
                || !value.IsNullOrUndefined() && value.StatusCode >= HttpStatusCode.OK && value.StatusCode < HttpStatusCode.MultipleChoices;
        }
    }
}
