﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Domain.Utils.Http.Contracts;

namespace Domain.Entities.HttpClient.Entities
{
    [DataContract]
    public class RichHttpClientResponse<T> : ICacheResponse
    {
        [DataMember]
        private T value;

        public T Value
        {
            get => value;
        }

        [DataMember]
        private HttpStatusCode statusCode;

        public HttpStatusCode StatusCode
        {
            get => statusCode;
        }

        private TimeSpan timeTaken;

        public TimeSpan TimeTaken
        {
            get => timeTaken;
        }

        private RichHttpClientResponse()
        {

        }

        public static async Task<RichHttpClientResponse<T>> BuildResponse(HttpResponseMessage message, TimeSpan timeTaken)
        {
            RichHttpClientResponse<T> response = null;

            if (message != null)
            {
                if (message.IsSuccessStatusCode)
                {
                    string plainResponse = await message.Content.ReadAsStringAsync().ConfigureAwait(false);

                    response = BuildResponse(message.StatusCode, JsonConvert.DeserializeObject<T>(plainResponse), timeTaken);
                }
                else
                {
                    response = BuildErrorResponse(message.StatusCode, timeTaken);
                }
            }

            return response;
        }

        public static RichHttpClientResponse<T> BuildResponse(HttpStatusCode statusCode, T value, TimeSpan timeTaken)
        {
            return new RichHttpClientResponse<T>()
            {
                statusCode = statusCode
                ,
                value = value
                ,
                timeTaken = timeTaken
            };
        }

        public static RichHttpClientResponse<T> BuildErrorResponse(HttpStatusCode statusCode, TimeSpan timeTaken)
        {
            return new RichHttpClientResponse<T>()
            {
                statusCode = statusCode
            };
        }

        #region ICacheResponse implementation

        public bool IsValidForCache
        {
            get => statusCode >= HttpStatusCode.OK
                   && statusCode < HttpStatusCode.InternalServerError
                   && statusCode != HttpStatusCode.RequestTimeout && Value != null;
        }

        public bool IsValidForAvailability
        {
            get
            {
                return IsValidForCache;
            }
        }

        public Guid Token { get; set; }

        public string GeneratedCacheKey { get; set; }

        public bool retrievedFromCache = false;
        public bool RetrievedFromCache
        {
            get => retrievedFromCache;
            set => retrievedFromCache = value;
        }

        #endregion
    }
}
