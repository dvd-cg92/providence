﻿using Domain.Entities.HttpClient.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Entities.HttpClient
{
    public interface IHttpRepository
    {
        Task<RichHttpClientResponse<T>> Get<T>(string path, int timeout, IDictionary<string, string> headers = null);
        Task<RichHttpClientResponse<T>> Post<T>(string path, object body, int timeout);
        Task<RichHttpClientResponse<T>> Put<T>(string path, object body, int timeout);
        void SetReferrer(string referrerUrl);
        void SetHeaders(string referrerUrl);
    }
}
