﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Domain.Contracts.Repository;
using Infrastructure.Repository;
using System;

namespace Api
{
    public static class ServicesCollectionExtensions
    {
        public static IServiceCollection RegisterMappers(this IServiceCollection services)
        {

            return services;
        }

        public static IServiceCollection RegisterRepositories(this IServiceCollection services)
        {

            return services;
        }

        public static IServiceCollection RegisterServices(this IServiceCollection services)
        {

            return services;
        }

        public static IServiceCollection RegisterFactories(this IServiceCollection services)
        {

            return services;
        }

        public static IServiceCollection RegisterHttpClients(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddHttpClient<IChartRepository, ChartRepository>(client =>
            { 
                client.BaseAddress = new Uri(configuration["Http:Clients:Rapidapi:BaseUrl"]);
                client.Timeout = TimeSpan.FromMilliseconds(int.Parse(configuration["Http:Clients:Rapidapi:TimeOut"]));
            });

            return services;
        }

        public static IServiceCollection RegisterUtilities(this IServiceCollection services,
            IConfiguration configuration)
        {

            return services;
        }
    }
}