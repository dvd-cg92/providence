﻿using Domain.Contracts.Repository;
using Domain.Entities.PresentationLayer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Providence.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ChartController : ControllerBase
    {
        private readonly ILogger<ChartController> _logger;
        private readonly IChartRepository _chartRepository;

        public ChartController(ILogger<ChartController> logger, 
            IChartRepository chartRepository)
        {
            _logger = logger;
            _chartRepository = chartRepository;
        }

        [HttpGet("ticker/{ticker}")]
        public async Task<IActionResult> Get(string ticker, [FromQuery] string interval = "5m")
        {
            if (string.IsNullOrWhiteSpace(ticker) || string.IsNullOrWhiteSpace(interval)) {
                return BadRequest("Invalid input");
            }

            var chart = await _chartRepository.GetChart(ticker, interval);

            return Ok(chart);
        }

        [HttpGet("ticker/{ticker}/show")]
        public async Task<IActionResult> GetShowEntity(string ticker, [FromQuery] string interval = "5m")
        {
            if (string.IsNullOrWhiteSpace(ticker) || string.IsNullOrWhiteSpace(interval))
            {
                return BadRequest("Invalid input");
            }

            var chart = await _chartRepository.GetChart(ticker, interval);
            var chartShowEntity = MapChartToShowEntity(chart);

            return Ok(chartShowEntity);
        }

        private GetChartShowEntity MapChartToShowEntity(Domain.Entities.GetChart.GetChartRs chart)
        {
            var rs = new GetChartShowEntity();
            rs.ChartData = new List<ChartDataItem>();
            var chartResult = chart.Chart.Result.First();

            for (int i = 0; i < chartResult.Timestamp.Length; i++)
            {
                var quote = chartResult.Indicators.Quote.First();
                var timeStampEpoch = chartResult.Timestamp[i];
                var timeStamp = DateTimeOffset.FromUnixTimeSeconds(timeStampEpoch);

                rs.ChartData.Add(new ChartDataItem()
                {
                    Close = quote.Close[i],
                    High = quote.High[i],
                    Low = quote.Low[i],
                    Open = quote.Open[i],
                    Volume = quote.Volume[i],
                    Date = timeStamp.DateTime
                });
            }

            return rs;
        } 
    }
}
