﻿using Domain.Entities.HttpClient;
using Domain.Entities.HttpClient.Entities;
using Domain.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Infrastructure.Utils.Http
{
    public class RichHttpClient : IHttpRepository, IDisposable
    {
        public readonly HttpClient HttpClient;

        public RichHttpClient(HttpClient httpClient)
        {
            HttpClient = httpClient;
        }

        public async Task<RichHttpClientResponse<T>> Get<T>(string path, int timeout, IDictionary<string, string> headers = null)
        {
            RichHttpClientResponse<T> response;

            Stopwatch watch = Stopwatch.StartNew();

            try
            {
                CancellationTokenSource cancellationToken =
                    new CancellationTokenSource(TimeSpan.FromMilliseconds(timeout));

                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, path);

                if (!headers.IsNullOrEmpty())
                {
                    foreach (KeyValuePair<string, string> h in headers)
                    {
                        request.Headers.Add(h.Key, h.Value);
                    }
                }

                HttpResponseMessage message = await HttpClient.SendAsync(request, cancellationToken.Token).ConfigureAwait(false);
                watch.Stop();
                response = await RichHttpClientResponse<T>.BuildResponse(message, watch.Elapsed).ConfigureAwait(false);

                TraceResult("GET", path, null, response, timeout, watch.Elapsed);
            }
            catch (Exception e)
            {
                response = GetErrorResponse<T>(e, watch.Elapsed);

                TraceResult("GET", path, null, response, timeout, watch.Elapsed);
            }

            return response;
        }

        public async Task<RichHttpClientResponse<T>> Post<T>(string path, object body, int timeout)
        {
            RichHttpClientResponse<T> response;

            Stopwatch watch = Stopwatch.StartNew();

            try
            {
                HttpContent content = new StringContent(body != null ? JsonConvert.SerializeObject(body) : string.Empty, System.Text.Encoding.UTF8, "application/json");

                CancellationTokenSource cancellationToken = new CancellationTokenSource(TimeSpan.FromMilliseconds(timeout));

                HttpResponseMessage message = await HttpClient.PostAsync(path, content, cancellationToken.Token).ConfigureAwait(false);
                watch.Stop();
                response = await RichHttpClientResponse<T>.BuildResponse(message, watch.Elapsed).ConfigureAwait(false);
            }
            catch (Exception e)
            {
                response = GetErrorResponse<T>(e, watch.Elapsed);

                TraceResult("POST", path, body, response, timeout, watch.Elapsed);

                string message =
                    $"{HttpClient.BaseAddress}/{path} | {e.Message}. {e.InnerException?.Message}. {e.InnerException?.InnerException?.Message}. StackTrace: {e.StackTrace}";

                //LogManager.SingleInstance.WriteTrace(message, "TransactionalBusinessApi.RichHttpClient", Guid.Empty, LogLevel.Error);
            }

            return response;
        }

        public async Task<RichHttpClientResponse<T>> Put<T>(string path, object body, int timeout)
        {
            RichHttpClientResponse<T> response;

            Stopwatch watch = Stopwatch.StartNew();

            try
            {
                HttpContent content = new StringContent(body != null ? JsonConvert.SerializeObject(body) : string.Empty, System.Text.Encoding.UTF8, "application/json");

                CancellationTokenSource cancellationToken = new CancellationTokenSource(TimeSpan.FromMilliseconds(timeout));

                HttpResponseMessage message = await HttpClient.PutAsync(path, content, cancellationToken.Token).ConfigureAwait(false);
                watch.Stop();
                response = await RichHttpClientResponse<T>.BuildResponse(message, watch.Elapsed).ConfigureAwait(false);

                TraceResult("PUT", path, body, response, timeout, watch.Elapsed);
            }
            catch (Exception e)
            {
                response = GetErrorResponse<T>(e, watch.Elapsed);

                TraceResult("PUT", path, body, response, timeout, watch.Elapsed);
                //LogManager.SingleInstance.WriteTrace(e.StackTrace, "TransactionalBusinessApi.RichHttpClient", Guid.Empty, LogLevel.Error);
            }

            return response;
        }

        public void SetReferrer(
            string referrerUrl)
        {
            HttpClient.DefaultRequestHeaders.Referrer = new Uri(referrerUrl);
        }

        public void SetHeaders(
            string referrerUrl)
        {
            HttpClient.DefaultRequestHeaders.Referrer = new Uri(referrerUrl);
        }

        private RichHttpClientResponse<T> GetErrorResponse<T>(Exception e, TimeSpan timeSpan)
        {
            RichHttpClientResponse<T> response = null;

            Exception tcex = null;

            if (e is AggregateException)
            {
                AggregateException aggex = e as AggregateException;

                tcex = aggex.InnerExceptions.FirstOrDefault(x => x is TaskCanceledException);
            }
            else if (e is TaskCanceledException)
            {
                tcex = e;
            }

            if (tcex != null)
            {
                // Timeout
                response = RichHttpClientResponse<T>.BuildErrorResponse(HttpStatusCode.RequestTimeout, timeSpan);
            }
            else
            {
                response = RichHttpClientResponse<T>.BuildErrorResponse(HttpStatusCode.InternalServerError, timeSpan);
            }

            return response;
        }

        private void TraceResult<T>(string method, string path, object body, RichHttpClientResponse<T> response, int timeout, TimeSpan duration)
        {
            string basepath = HttpClient.BaseAddress.ToString();

            string endpoint = $"{basepath.Substring(0, basepath.Length - 1)}{path}";

            Debug.WriteLine($"\t{method.ToUpper()} - {endpoint}");

            string bodySerialization = body == null ? "null" : JsonConvert.SerializeObject(body);
            string responseSerialization = response.IsNullOrUndefined() ? "null" : JsonConvert.SerializeObject(response.Value);

            decimal timeoutPercentage = (decimal)duration.TotalMilliseconds / timeout;

            //if (VerboseTracing)
            //{
            //    Debug.WriteLine($"\tBody: {bodySerialization}");
            //    Debug.WriteLine($"\tResponse: {(int)response.StatusCode} {response.StatusCode} - {responseSerialization}");
            //}

            Debug.WriteLine($"\tDuration: {duration.TotalMilliseconds}ms, timeout was {timeout}ms, {timeoutPercentage:P2}");

            Debug.WriteLine("---");

            //if (VerboseTracing)
            //{
            //    string message = $"Http request error: Endpoint: {endpoint}. Status code: {(int)response.StatusCode} {response.StatusCode.ToString()}. Timeout: {timeout}ms. Duration: {duration.TotalMilliseconds}ms ({timeoutPercentage:P2}).";

                //LogManager.SingleInstance.WriteTrace(message, "TransactionalBusinessApi.RichHttpClient", Guid.Empty, LogLevel.Error);

                //if (!bodySerialization.IsNullOrEmpty())
                //{
                //    LogManager.SingleInstance.WriteTrace($"Body: {bodySerialization}", "TransactionalBusinessApi.RichHttpClient", Guid.Empty, LogLevel.Error);
                //}

                //if (!responseSerialization.IsNullOrEmpty())
                //{
                //    LogManager.SingleInstance.WriteTrace($"Response: {responseSerialization}", "TransactionalBusinessApi.RichHttpClient", Guid.Empty, LogLevel.Error);
                //}
            //}
        }

        #region IDisposable Support
        private bool DisposedValue; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!DisposedValue)
            {
                if (disposing)
                {
                    HttpClient.Dispose();
                }
                DisposedValue = true;
            }
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion
        //private bool VerboseTracing => ConfigurationManager.SingleInstance
        //                                                   .AsString("Toolfactory.Packages.TransactionalBusinessApi.Communication.Tracing.Profile", "Low")
        //                                                   .ToLower() == "verbose";
    }
}
