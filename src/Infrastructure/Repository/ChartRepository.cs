﻿using Domain.Contracts.Repository;
using Domain.Entities.GetChart;
using Domain.Exceptions;
using Infrastructure.Utils.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class ChartRepository : RichHttpClient, IChartRepository
    {
        private readonly int _timeOut;
        private readonly string _apiKey;

        public ChartRepository(HttpClient httpClient, IConfiguration configuration) : base(httpClient)
        {
            _timeOut = int.Parse(configuration["Http:Clients:Rapidapi:TimeOut"]);
            _apiKey = configuration["Http:Clients:Rapidapi:ApiKey"];
        }

        public async Task<GetChartRs> GetChart(string ticker, string interval)
        {
            if (string.IsNullOrWhiteSpace(ticker) || string.IsNullOrWhiteSpace(interval))
            {
                throw new ArgumentNullException("Invalid parameters");
            }

            var uri = $"stock/v2/get-chart?interval={interval}&symbol={ticker}&range=1d&region=US";

            var headers = new Dictionary<string, string>
                {
                    { "x-rapidapi-key", _apiKey },
                };

            var rs = await Get<GetChartRs>(uri, _timeOut, headers);

            if (rs.IsValidForCache)
            {
                // TODO: CACHE
            }

            if (rs.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new ControlledException($"Unable to get chart of {ticker} with interval {interval}. GET: {uri}");
            }

            return rs.Value;
        }
    }
}
